(function () {
    'use strict';
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement('style');
        msViewportStyle.appendChild(
            document.createTextNode(
                '@-ms-viewport{width:auto!important}'
            )
        );
        document.querySelector('head').appendChild(msViewportStyle)
    }
})();

$(function($){
    var topnav = $('.header');
    var label = $('.label');
    $h = label.offset().top;

    $(window).scroll(function(){
        // Если прокрутили скролл ниже макушки блока, включаем фиксацию

        if ( $(window).scrollTop() > $h) {
            topnav.addClass('fix-top');
        }else{
            //Иначе возвращаем всё назад. Тут вы вносите свои данные
            topnav.removeClass('fix-top');
        }
    });
});

$('.topnav a').click(function(){
    var str=$(this).attr('href');
    $.scrollTo(str, 500, {offset: -80});
    return false;
});



$('.slider-item .btn').click(function(){
    var str=$(this).attr('href');
    $.scrollTo(str, 500, {offset: -80});
    return false;
});


// Navs

$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(100);
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 1170 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
});


$('.slider').slick({
    arrows: false,
    autoplay: true,
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slider-nav prev"><i class="fa fa-chevron-left"></i></span>',
    nextArrow: '<span class="slider-nav next"><i class="fa fa-chevron-right"></i></span>',

    responsive: [
        {
            breakpoint: 768,
            arrows: true
        }
    ]
});


$(".btn-modal").fancybox({
    'padding'    : 0,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn-close" href="javascript:;"><i class="fa fa-times"></i></a>'
    }
});


$('.review').slick({
    autoplay: false,
    dots: false,
    arrows: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="review-nav prev"><i class="fa fa-angle-left"></i></span>',
    nextArrow: '<span class="review-nav next"><i class="fa fa-angle-right"></i></span>',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }
    ]
});


$('.partners').slick({
    autoplay: false,
    dots: false,
    arrows: false,
    mobileFirst: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        }
    ]
});

// Map

ymaps.ready(init);

var myMap,
    myPlacemark;

function init(){
    myMap = new ymaps.Map("map", {
        center: [58.0420,56.1034],
        zoom: 12,
        controls: ['smallMapDefaultSet']
    });



    myMap.geoObjects
        .add(new ymaps.Placemark([58.0420,56.1034], {
            balloonContent: '<strong>Офис</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [47, 69],
            iconImageOffset: [-23, -70]
        }))
        .add(new ymaps.Placemark([58.0220,56.1688], {
            balloonContent: '<strong>Чукаевский карьер</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [47, 69],
            iconImageOffset: [-23, -70]
        }));

    myMap.behaviors.disable('scrollZoom');
    myMap.behaviors.disable('multiTouch');

}


$(document).ready(function(){


    $('.btn-send').click(function() {

        $('body').find('form:not(this)').children('div').removeClass('red'); //удаление всех сообщение об ошибке(валидатора)
        var answer = checkForm($(this).closest('form').get(0)); //ответ от валидатора
        if(answer != false)
        {
            var $form = $(this).closest('form'),
                product    =     $('select[name="product"]', $form).val(),
                weight    =     $('input[name="weight"]', $form).val(),
                name    =     $('input[name="name"]', $form).val(),
                phone   =     $('input[name="phone"]', $form).val(),
                email   =     $('input[name="email"]', $form).val();
            $.ajax({
                type: "POST",
                url: "form-handler.php",
                data: {name: name, phone: phone, product:product, email: email, weight:weight}
            }).done(function(msg) {
                $('form').find('input[type=text], textarea').val('');
                console.log('удачно');
                document.location.href = "./done.html";
            });
        }
    });


});
